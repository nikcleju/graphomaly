========================
Graphomaly Preprocessing
========================


Egonet
~~~~~~
.. automodule:: preprocessing.egonet
    :members:

Random walk
~~~~~~~~~~~
.. automodule:: preprocessing.rwalk
    :members:

Spectrum
~~~~~~~~
.. automodule:: preprocessing.spectrum
    :members:

Graph to features
~~~~~~~~~~~~~~~~~
.. automodule:: preprocessing.graph_to_features
    :members:

Transactions to graph
~~~~~~~~~~~~~~~~~~~~~
.. automodule:: preprocessing.transactions_to_graph
    :members:

Time-related features
~~~~~~~~~~~~~~~~~~~~~
.. autoclass:: preprocessing.fe_base.time_features.TimeFeatures
    :members:

.. autoclass:: preprocessing.fe_time_transformers.TimeFeatTransformer
    :members:

.. autoclass:: preprocessing.fe_base.time_features.GroupHistoricalTimeFeatures
    :members:

.. autoclass:: preprocessing.fe_time_transformers.GroupHistTimeFeatTransformer
    :members:

Group statistical features
~~~~~~~~~~~~~~~~~~~~~~~~~~
.. autoclass:: preprocessing.fe_base.statistical_features.GroupStatisticalFeatures
    :members:

.. autoclass:: preprocessing.fe_statistical_transformer.GroupStatisticalFeatTransformer
    :members:

Historical features
~~~~~~~~~~~~~~~~~~~
.. automodule:: preprocessing.fe_base.historical_features
    :members:

.. automodule:: preprocessing.fe_difference_transformer
    :members:
