=================
Graphomaly Models
=================


Autoencoder
~~~~~~~~~~~

.. automodule:: models.autoencoder.Autoencoder
   :members:
   :undoc-members:
   :show-inheritance:


Dictionary Learning
~~~~~~~~~~~~~~~~~~~
.. autoclass:: models.dictlearn.AnomalyDL
   :members:


Isolation Forest
~~~~~~~~~~~~~~~~

.. automodule:: models.sklearn.SklearnIsolationForest
   :members:
   :undoc-members:
   :show-inheritance:


Variational Autoencoder
~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: models.vae.VAE
   :members:
   :undoc-members:
   :show-inheritance:


.. automodule:: models.vae.Sampling
   :members:
   :undoc-members:
   :show-inheritance:
