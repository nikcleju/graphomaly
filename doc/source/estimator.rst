====================
Graphomaly Estimator
====================


Estimator
~~~~~~~~~

.. autoclass:: graphomaly.estimator.GraphomalyEstimator
   :members:
   :undoc-members:
   :show-inheritance:


Grid Search
~~~~~~~~~~~

.. autoclass:: graphomaly.grid_search.GridSearch
   :members:
   :undoc-members:

Voting Classifier
~~~~~~~~~~~~~~~~~

.. autoclass:: graphomaly.voting.VotingClassifier
   :members:
   :undoc-members:
   :show-inheritance:

