# Copyright (c) 2022 Stefania Budulan <stefania.budulan@tremend.com>
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

import pandas as pd
import numpy as np
from numpy import random
from os.path import exists
from sklearn import set_config
from sklearn.datasets import fetch_openml
from sklearn.pipeline import Pipeline
from sklearn.compose import ColumnTransformer
from sklearn.preprocessing import FunctionTransformer
from sklearn.pipeline import FeatureUnion

# Set random seed
random.seed(28121991)

from graphomaly.preprocessing.fe_time_transformers import TimeFeatTransformer
from graphomaly.preprocessing.fe_time_transformers import GroupHistTimeFeatTransformer
from graphomaly.preprocessing.fe_difference_transformer import DifferenceFeatTransformer
from graphomaly.preprocessing.fe_statistical_transformer import (
    GroupStatisticalFeatTransformer,
)


def gen_date(group):
    """Generate a timestamp starting with one of the days
    in January 2022, of the length of the group received as param.
    """
    return pd.date_range(
        start="2022-01-" + str(random.randint(1, 32)), periods=group.shape[0], freq="D"
    )


def obtain_dataset(data_filename, instance_no=None):
    df = pd.read_csv(data_filename)

    # Retrieve a subset of the instances
    # Some ID destinations may not have data. The real-life interpretation
    #    would be that those recipicients are from outside our bank.
    df = df.head(instance_no)

    # Generate a timestamp column with consecutive days for each ID source
    # It takes about 15sec.
    df["date"] = df["id_source"].groupby(df["id_source"]).transform(gen_date)

    return df


# Read the dataset as DataFrame
# The dataset employed here is available at http://graphomaly.upb.ro/,
#    under the description "A synthetic transaction graph"
# This dataset is employed here solely for the purpose of Feature Engineering
#    exemplification.
synthetic_data_file = "./data/G12/G12.csv"
if not exists(synthetic_data_file):
    raise FileNotFoundError(
        "You must download the 'synthetic transaction graph' from http://graphomaly.upb.ro/ "
        "and save it as ./data/G12/G12.csv, or change the data file path in the script."
    )
    exit()
df = obtain_dataset(synthetic_data_file, instance_no=10000)
print(f"Initial dataset:\n{df.head()}")


initial_featunion = FeatureUnion(
    [
        (
            "time_ct",
            ColumnTransformer(
                [
                    (
                        "time_feats",
                        TimeFeatTransformer(
                            feature_list=["day_of_month", "day_of_week"]
                        ),
                        ["date"],
                    )
                ],
                remainder="passthrough",
            ),
        ),
        (
            "group_ct",
            ColumnTransformer(
                [
                    (
                        "hist_feats",
                        GroupHistTimeFeatTransformer(
                            units=["h"], group_suffix="id_src"
                        ),
                        ["date", "id_source"],
                    ),
                    (
                        "stat_feats",
                        GroupStatisticalFeatTransformer(
                            feature_list=["mean", "min"],
                            units=["7D"],
                            group_suffix="id_src",
                        ),
                        ["date", "id_source", "cum_amount"],
                    ),
                ]
            ),
        ),
    ]
)

# Apply the FeatureUnion pipeline and clean the results
set_config(display="text")
print(f"Initial FeatureUnion pipeline:\n{initial_featunion}")

X_r = initial_featunion.fit_transform(df)
init_fu_cols = initial_featunion.get_feature_names_out()
cols_clean = [col.split("__")[-1] for col in init_fu_cols]
df_fu = pd.DataFrame(X_r, columns=cols_clean)
print(f"Feature names for the initial feature union: \n{cols_clean}")


# From the initial generated features, we can now compute
#    historical difference for each instance.
# For example, compute as feature the difference between:
#   1. The cum_amount and the min cum_amount in the past 7 days.
#   2. The cum_amount and the mean cum_amount in the past 7 days.

pipeline = ColumnTransformer(
    [
        (
            "diff1",
            DifferenceFeatTransformer(
                first_col="amount", second_col="amount_min_7D_id_src"
            ),
            ["cum_amount", "cum_amount_mean_7D_id_src"],
        ),
        (
            "diff2",
            DifferenceFeatTransformer(
                first_col="amount", second_col="amount_min_7D_id_src"
            ),
            ["cum_amount", "cum_amount_min_7D_id_src"],
        ),
    ],
    remainder="passthrough",
)


# Apply the ColumnTransformer pipeline and clean the results
X_r = pipeline.fit_transform(df_fu)
pipeline_cols = pipeline.get_feature_names_out()
pipe_cols_clean = [col.split("__")[-1] for col in pipeline_cols]
df_out = pd.DataFrame(X_r, columns=pipe_cols_clean)

print(f"ColumnTransformer pipeline:\n{pipeline}")
print(f"Feature names for the final processing: \n{pipe_cols_clean}")
print(f"Processed dataset:\n{df_out.head()}")
