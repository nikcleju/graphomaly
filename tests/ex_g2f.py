# example of using graph to features methods

import time

import egonet
import numpy as np
import pandas as pd
import rwalk

graph_file_name = "../synth_graph_v11.csv"  # file where to find the graph in csv format
X = pd.read_csv(graph_file_name)  # read graph as dataframe
X = X.to_numpy()  # comment this line if you want to work with dataframes

# Make class instances for all desired types of features
iego = egonet.EgonetFeatures(save_base_features=True, verbose=True)
irwalk = rwalk.RwalkFeatures(
    rwalk_len=5, rwalk_reps=20, save_base_features=True, verbose=True
)

# define some feature dictionaries of lists
ego_feature_lists = [
    [
        "f_degree_in",
        "f_degree_out",
        "f_amount_in",
        "f_amount_out",
        "f_ego_edge_density",
    ],  # list 0: egonet only
    [
        "f_degree_in",
        "f_degree_out",
        "f_amount_in",
        "f_amount_out",
        "f_ego_edge_density",
        "f_egored_degree_in_rel",
        "f_egored_degree_out_rel",
        "f_egored_amount_in_rel",
        "f_egored_amount_out_rel",
        "f_egored_edge_density",
    ],  # list 1: egonet+egored
]

rwalk_feature_lists = [
    ["f_rwalk_transfer_out", "f_rwalk_ring_max"],
    ["f_rwalk_transfer_in", "f_rwalk_transfer_out", "f_rwalk_ring_max"],
]

# Compute all combinations of features
# Note that the computation time in all iterations but the first is very small.
# This is because the base features were saved when the first lists were computed
for kego in range(len(ego_feature_lists)):
    for krw in range(len(rwalk_feature_lists)):
        startTime = time.time()
        ego_node_features = iego.fit_transform(X, feature_list=ego_feature_lists[kego])
        rwalk_node_features = irwalk.fit_transform(
            X, feature_list=rwalk_feature_lists[krw]
        )
        stopTime = time.time()

        # join features, then apply your preferred anomaly detection algorithm
        all_node_features = np.concatenate(
            (ego_node_features, rwalk_node_features), axis=1
        )
        print(
            "Features ego list ",
            kego,
            ", rwalk feature list ",
            krw,
            ". Time=",
            stopTime - startTime,
            ", shape=",
            all_node_features.shape,
            sep="",
        )
