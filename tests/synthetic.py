# Copyright (c) 2021, 2022 Paul Irofti <paul@irofti.net>
# Copyright (c) 2020 Andra Băltoiu <andra.baltoiu@gmail.com>
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

from graphomaly.estimator import GraphomalyEstimator
from pyod.utils.data import generate_data


class SyntheticEstimator(GraphomalyEstimator):
    def __init__(
        self,
        **pparams,
    ):
        super(SyntheticEstimator, self).__init__(**pparams)

    def load(self):
        X, X_test, y, y_test = generate_data(
            n_train=self.config.dataset.n_train,
            n_test=self.config.dataset.n_test,
            n_features=self.config.dataset.n_features,
            contamination=self.config.dataset.contamination,
            random_state=42,
            behaviour="new",
        )

        return X, y, X_test, y_test
